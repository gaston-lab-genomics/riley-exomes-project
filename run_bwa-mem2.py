#!/usr/bin/env python

# Standard packages
import sys
import csv
import argparse
import subprocess

from collections import defaultdict

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    args = parser.parse_args()
    args.logLevel = "INFO"

    bwa_cmd = f"/mnt/shared-data/Software/bwa-mem2/bwa-mem2"
    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"
    # ref = f"/mnt/shared-data/Resources/ReferenceData/Mouse/GRCm39.fasta"

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            sample = row[0]
            fastq1 = row[1]
            fastq2 = row[2]

            tmp = f"{sample}.tmp"
            bam = f"{sample}.bwa_aligned.bam"

            sys.stdout.write(
                f"Running:  {bwa_cmd} mem -t 24 {ref} {fastq1} {fastq2} | samtools view -u - | samtools sort -@ 24 -O bam -o {bam} -T {tmp} -\n"
            )
            subprocess.run(
                f"{bwa_cmd} mem -t 24 {ref} {fastq1} {fastq2} | samtools view -u - | samtools sort -@ 24 -O bam -o {bam} -T {tmp} -", stderr=subprocess.PIPE, shell=True
            )
