#!/usr/bin/env python

# Standard packages
import sys
import csv
import argparse
import subprocess

from multiprocessing import Pool
from collections import defaultdict

def execute_cmd(cmd_string):
    sys.stdout.write(
        f"Running: {cmd_string}\n"
    )
    subprocess.run(
        f"{cmd_string}", stderr=subprocess.PIPE, shell=True
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help="Input file for samples")
    args = parser.parse_args()
    args.logLevel = "INFO"

    ref = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/Homo_sapiens_assembly38.fasta"
    pool = Pool(3)
    recal_cmds = []
    commands = []

    with open(args.input, 'r') as csvfile:
        reader = csv.reader(csvfile, delimiter='\t')
        header = next(reader)
        for row in reader:
            sample = row[0]
            recalibrated = f"{sample}.recalibrated.bam"
            gvcf = f"{sample}.haplotypecaller.g.vcf.gz"

            cmd_string = f"gatk HaplotypeCaller --native-pair-hmm-threads 8 -R {ref} -I {recalibrated} -O {gvcf} -ERC GVCF\n"
            commands.append(cmd_string)

        results = pool.map(execute_cmd, commands)
