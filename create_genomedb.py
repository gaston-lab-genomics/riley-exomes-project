#!/usr/bin/env python

# Standard packages
import sys
import csv
import glob
import argparse
import subprocess

from collections import defaultdict

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', help="Output name for database. Will create directory")
    args = parser.parse_args()
    args.logLevel = "INFO"

    db = f"./{args.output}"
    tmp = f"./{args.output}_tmp"

    intervals_file = f"/mnt/shared-data/Resources/ReferenceData/GRCh38/wgs_calling_regions.hg38.interval_list"
    gvcfs = glob.glob('*.g.vcf.gz')

    start = 0
    length = len(gvcfs)
    for i in range(start, length, 20):
        samples = gvcfs[i:i+20]

        sample_string = " -V ".join(samples)
        if i == 0:
            cmd_string = f"""gatk --java-options "-Xmx100g -Xms24g" GenomicsDBImport --genomicsdb-shared-posixfs-optimizations --max-num-intervals-to-import-in-parallel 12 -V {sample_string} --genomicsdb-workspace-path {db} --tmp-dir {tmp} -L {intervals_file}"""
        else:
            cmd_string = f"""gatk --java-options "-Xmx100g -Xms24g" GenomicsDBImport --genomicsdb-shared-posixfs-optimizations --max-num-intervals-to-import-in-parallel 12 -V {sample_string} --genomicsdb-update-workspace-path {db} --tmp-dir {tmp} -L {intervals_file}"""

        sys.stdout.write(
            f"Running: {cmd_string}\n"
        )
        subprocess.run(
            f"{cmd_string}", stderr=subprocess.PIPE, shell=True
        )
